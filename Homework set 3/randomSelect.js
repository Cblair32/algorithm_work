var fs = require("fs");
var text = fs.readFileSync("./kargerMinCut.txt").toString('utf-8');
var temp = text.split("\n");
function adjacency(array){
  var final = [];
  for(var i = 0; i<array.length; i++){
    var temp = array[i].split("\t");
    temp.pop();
    final.push(temp);
  }
  return final;
}
//Implement random selection of edge
//implement merge/contract function(What does it take to remove one of the points? Merge edges from both points?)
//figure out how to remove self-loops
//
function contraction(array){
  var v = array.length;
  var used = [];
  while(v>2){
    //pick a remaining edge (u,v) uniformly @ random
    var randomV = Math.floor(Math.random()*array.length-1);
    var randomE = Math.floor(Math.random()*array[randomV].length-1);
    var V = array[randomV];
    var E = V[randomE];
    var Com1 = array[V];
    var Com2 = array[E];
    var N = [];
    array.splice(randomV);
    array.splice(randomE);
    N.push(Com1);
    N.push(Com2);
    //remove self loops
    for(var i = 0; i<N.length; i++){
      var j = i+1;
      if(array[j][i]==array[j][0]){
        N.splice(i);
      }
    }
    //return cut "Represented by" final 2 vertices
    return array;
  }
}
var array = adjacency(temp);
//console.log(adjacency(array));
//console.log(test2);
//console.log(test3);
//console.log(array);
//.log(array[199][0]);
console.log(contraction(array).length);
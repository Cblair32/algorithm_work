var fs = require("fs");
var text = fs.readFileSync("./QuickSort.txt").toString('utf-8');
var array = text.split("\r\n");

function swap(array, firstIndex, secondIndex) {
  var temp = array[firstIndex];
  array[firstIndex] = array[secondIndex];
  array[secondIndex] = temp;
}

function isOdd(num) {
    return num % 2;
}

function getMiddleElemIdx(arr, start, end) {
    const length = end - start + 1;
    if (isOdd(length)) {
        return start + Math.floor(length / 2);
    } else {
        return start + Math.floor(length / 2) - 1;
    }
}

function choosePivotMedian(array, start, end) {
    const first = array[start];
    const middleIdx = getMiddleElemIdx(array, start, end);
    const middle = array[middleIdx];
    const last = array[end];
    if ((first < middle && middle < last) || (last < middle && middle < first)) {
        return middleIdx;
    } else if ((middle < first && first < last) || (last < first && first < middle)) {
        return start;
    } else {
        return end;
    }
}

var counter = 0;

function quickSort(array, start, end) {
  if(start<end){
    var pivotIndex = choosePivotMedian(array,start,end);
    swap(array,start,pivotIndex);
    var pivot = array[start];
    //console.log(pivot);
    //console.log(array);
    var i = start+1;
    for(var j = start+1; j<=end; j++){
      if(parseInt(array[j]) < pivot){
        swap(array,i,j);
        i++;
      }
     
      counter++;
    }
    //console.log(array);
    swap(array,start,(i-1));
    var temp = quickSort(array,start,i-2);
    var temp2 = quickSort(array,i,end);
    //console.log(temp);
    return counter
  } else{
    return 0;
  }
}





//var array = [9, 7, 5, 11, 12, 2, 14, 3, 10, 6];
//var array = [9,8,7,6,5,4,3,2,1];
//var array = [9,8,7,6,5,4,3,2,1,10,11,12,13,14,15];
console.log(quickSort(array,0,array.length-1));
//quickSort(array,0,array.length-1);
//console.log(array);
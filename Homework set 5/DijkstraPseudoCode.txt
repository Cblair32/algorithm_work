Initialize:

X = [];  (Vertices processed so far)

A[s] = 0 (computed shortest path distances)


Main Loop:

While X!=V
  - among all edges (v,w) w/ v in X and all W not in X
  pick the edge that minimizes Dijkstra's greedy criterion 
  A[v] + lvw = (v*, w*)
  - add w* to X
  -set A[w*] = A[v*]+lv*w*
  
/* The file contains an adjacency list representation of an undirected weighted graph with 200 vertices labeled 1 to 200. 
Each row consists of the node tuples that are adjacent to that particular vertex along with the length of that edge. For example, 
the 6th row has 6 as the first entry indicating that this row corresponds to the vertex labeled 6. 
The next entry of this row "141,8200" indicates that there is an edge between vertex 6 and vertex 141 that has length 8200. 
The rest of the pairs of this row indicate the other vertices adjacent to vertex 6 and the lengths of the corresponding edges.

Your task is to run Dijkstra's shortest-path algorithm on this graph, using 1 (the first vertex) as the source vertex, 
and to compute the shortest-path distances between 1 and every other vertex of the graph. If there is no path between a vertex v and vertex 1,
we'll define the shortest-path distance between 1 and v to be 1000000.

You should report the shortest-path distances to the following ten vertices, in order: 7,37,59,82,99,115,133,165,188,197. 
Enter the shortest-path distances using the fields below for each of the vertices.
 */
var fs = require("fs");
var text = fs.readFileSync("./test.txt").toString('utf-8');
var temp = text.split("\n");

function split(array){
  var final = [];
  for(var i=0; i<array.length; i++){
    var input = array[i].split(" ");
    final.push(input);
  }
  return final;
}
var between = split(temp);

function organize(array){
  //Initializes the data for the main while loop
  var final = [];
  var temp;
  var alt;
  var target = {};
  for(var i = 0; i<array.length; i++){
    target={};
    for(var j = 0; j<array[i].length; j++){
      if(j==0){
        temp = array[i][j];
      }
      else{
        alt = array[i][j].split(',');
        target[alt[0]]=alt[1];
      }
    }
    if(target.length!==0){
      final.push(target);
    }
    
  }
  return final;
}

var graph = organize(between);

function isIn(array,item){
  for(var i = 0; i<array.length;i++){
    if(array[i]===item){
      return true;
    }
  }
  return false;
}


// Main function
function shortestPath(array,start,end){
  var X = [];
  var A = [];
  var s = start-1;
  var edge;
  var path = 0;
  var min = Infinity;
  var prev = s;
  var next;
  var increment;
  A[s] = 0;
  while(X.length <= array.length){//while X!= V
    edge = array[prev];
    min = Infinity;
    for(var key in edge){//among all edges (v,w) w/ v in X and all W not in X pick the edge that minimizes Dijkstra's greedy criterion 
    //console.log(key);
      if(isIn(X,edge[key])){
        //check if the key is in X
        //if it is, continue onto the next key
        continue;
      }
      //Currently calculating correct path numbers, but not going on correct path.
      if(parseInt(edge[key])<min){
        min = edge[key];
        //console.log(min);
        next = key;
        //console.log(next);
      }
      prev = key-1;
      
    }
    //increment = parseInt(increment);
    //console.log(increment);
    min = parseInt(min);
    console.log(path);
    path+=min;
    //console.log(min);
    X.push(key);
    A[key] = path;
    //console.log(A);
    if(key==end){
      break;
    }
  }
  return A[end];
}

console.log(shortestPath(graph,1,15));

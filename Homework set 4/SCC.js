/*
3 steps
1. Let Grev = G with all arc reversed
2. run DFSLoop on Grev
3. run DFSLoop on G
*/
var fs = require("fs");
var text = fs.readFileSync("./Homework set 4/SCC.txt").toString('utf-8');
var temp = text.split("\n");

function vertex(num,arc){
  //create vertex object constructoor
  this.vertex = num;
  this.arc = [arc];
  this.explored = false;
  this.finishing_time = 0;
  this.leader = 0;
}

function graphRep(array){
  //creates adjacency list
  var final = [];
  for(var i = 0; i<array.length; i++){
    var temp = array[i].split(" ");
    temp.pop();
    //console.log(temp);
    if(i==0){
      var token = new vertex(temp[0], temp[1]);
      final.push(token);
    }
    else if(final[final.length-1].vertex==temp[0]){
      final[final.length-1].arc.push(temp[1]);
    }
    else{
      token = new vertex(temp[0], temp[1]);
      final.push(token);
    }
  }
  return final;
}

function reverseGraph(array){
  // creates reversed graph
  array.reverse();
  var sub = [];
  var final = [];
  for(var i = 0; i<array.length; i++){
    var temp = array[i].split(" ");
    temp.pop();
    sub.push(temp);
    sub[i].reverse();
  }
  sub.sort();
  for(var j = 0; j<sub.length; j++){
    if(j==0){
      var token = new vertex(sub[j][0], sub[j][1]);
      final.push(token);
    }
    else if(final[final.length-1].vertex==sub[j][0]){
      final[final.length-1].arc.push(sub[j][1]);
    }
    else{
      token = new vertex(sub[j][0], sub[j][1]);
      final.push(token);
    }
  }
  
  return final;
}

//create graph

//create supplementary graph for iterations
var graphFor = graphRep(temp);

//create reversed graph
var graphRev = reverseGraph(temp);


//Initialize variable for finishing times
var t = 0;

//Initialize variable for current vertex
var s=0;
function DFSLoop(graph){
  //first call on graphRev
  //During DFS call on graphRev, assign finishing times to graphFor
  //in between first and second call. Sort graphFor by finishing times
  //Second call on graphFor
  for(var i = graph.length-1; i>0; i--){
    if(graph[i].explored === false){
      s = i+1;
      DFS(graph,i);
      //console.log(s);
    }
  }
}

//All vertexes are being labeled with 9. Need to figure out how to make sure that doesn't happen
function DFS(graph, i){
  graph[i].explored = true;
  graph[i].leader = s;
  var index;
  //For each arc in graph[i]
  for(var j = 0; j<graph[i].arc.length; j++){
    var edge = graph[i].arc[j];
    //iterate through graph, find when edge is equal to a vertex in the graph, take that index and pop it into the below control flow
    for(var x = 0; x<graph.length; x++){
      var test = graph[x].vertex;
      if(test==edge){
        index = x;
      }
    }
    if(graph[index].explored == false){
      DFS(graph,index);
    }
  }
  t++;
  //assign finishing times to forward graph
  graphFor[i].finishing_time = t;
}


DFSLoop(graphRev);
//console.log(graphRev);
graphFor.sort(function(a,b){return a.finishing_time-b.finishing_time});
DFSLoop(graphFor);
//console.log(graphFor);

function SCC(leader,list){
  //create SCC object constructor to track size of SCCs
  this.leader = leader;
  this.list = [list];
}
function isIn(graph,item){
  for(var i = 0; i<graph.length; i++){
    if(graph[i].leader == item){
      return true;
    }
  }
}

function createSccArray(graph){
  //iterate through graph and compile a list of SCCs and how many vertexes are a part of them
  var final = [];
  for(var i = 0; i<graph.length; i++){
    var sample = graph[i].leader;
    if(isIn(final,sample)){
      //If the SCC is already added, add the vertex to the SCC's follower property
      for(var j = 0; j<final.length; j++){
        if(final[j].leader == sample){
          final[j].list.push(graph[i].vertex);
        }
      }
    }
    else{
      var token = new SCC(graph[i].leader, graph[i].vertex);
      final.push(token);
    }
  }
  for(var x = 0; x<final.length; x++){
    final[x].size = final[x].list.length;
  }
  final.sort(function(a,b){return b.size-a.size});
  return final;
}
console.log(createSccArray(graphFor));


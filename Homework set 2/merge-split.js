//var fs = require("fs");
//var text = fs.readFileSync("./IntegerArray.txt").toString('utf-8');
//var array = text.split("\n")
var merge = function(array, p, q, r) {
    var lowHalf = [];
    var highHalf = [];

    var k = p;
    var i;
    var j;
    var counter = 0;
    for (i = 0; k <= q; i++, k++) {
        lowHalf[i] = array[k];
    }
    for (j = 0; k <= r; j++, k++) {
        highHalf[j] = array[k];
    }

    k = p;
    i = 0;
    j = 0;
    
    // Repeatedly compare the lowest untaken element in
    //  lowHalf with the lowest untaken element in highHalf
    //  and copy the lower of the two back into array
    while(i<lowHalf.length && j<highHalf.length) {
     if(highHalf[j] < lowHalf[i]){
        array[k] = highHalf[j];
        j++;
     } else {
         array[k] = lowHalf[i];
         i++;
         counter ++;
     }
    k++;
    }
    // Once one of lowHalf and highHalf has been fully copied
    //  back into array, copy the remaining elements from the
    //  other temporary array back into the array
    while(i<lowHalf.length) {
        array[k] = lowHalf[i];
        k++;
        i++;
    }

    while(j<highHalf.length) {
        array[k] = highHalf[j];
        k++;
        j++;
    }
    
    return counter;
};
var mergeSort = function(array, p, r) {
    if(p<r){
     var q = Math.floor((p+r)/2);   
     var B = mergeSort(array,p,q);
     var C = mergeSort(array,q+1, r);
     var Z = merge(array, p, q ,r);
    }
    return Z;
};
var array = [10,9,8,7,6,5,4,3,2,1];
//var array = [1,3,5,2,4,6];
//console.log(merge(array, 0, Math.floor((0 + array.length-1) / 2), array.length-1));
console.log(mergeSort(array, 0, array.length-1));